﻿/// <reference path="qunitTestDependencies.js"/>
/// <reference path="../../../JsTestExamples/Scripts/employees/employeeSearch.js"/>

// surrounding tests with this to hide calcVm from global scope
(function () {
    var searchVm;

    QUnit.module("QUnitEmployeeSearchViewModel",
    {
        beforeEach: function () {
            searchVm = new EmployeeSearchViewModel();
        },
        afterEach: function () {
            $.mockjax.clear();
        }
    });

    QUnit.test("getSearchResults makes a request with the search query", function (assert) {

        var done = assert.async();

        $.mockjax({
            url: "/Employee/Search",
            status: 200,
            responseText: [
                { id: 0, name: "Jordan Walters" },
                { id: 1, name: "David Richards" }
            ]
        });

        var ajaxPromise = searchVm.getSearchResults();

        ajaxPromise.then(function (data) {
            // success
            var results = searchVm.results();

            assert.strictEqual(results.length, 2);

            var firstResult = results[0];
            assert.strictEqual(firstResult.name, "Jordan Walters");
            done();
        }, function (jqXHR, textStatus, error) {
            // failure
            assert.notOk(jqXHR, "request failed unexpectedly");
            done();
        });
    });

    QUnit.test("getSearchResults handles failed requests", function (assert) {

        var done = assert.async();

        $.mockjax({
            url: "/Employee/Search",
            status: 500,
            responseText: "Ya done goofed!"
        });

        var ajaxPromise = searchVm.getSearchResults();

        ajaxPromise.then(function (data) {
            // success
            assert.notOk("Search succeeded, miraculously.");
            done();
        }, function (jqXHR, textStatus, error) {
            // failure
            assert.strictEqual(jqXHR.status, 500);

            assert.ok(searchVm.searchError());
            done();
        });
    });

    QUnit.test("activeEmployeeSalaries returns empty array given no search results", function (assert) {
        searchVm.results([]);

        var result = searchVm.activeEmployeeSalaries();

        assert.strictEqual(result.length, 0);
    });

    QUnit.test("activeEmployeeSalaries returns empty array given no search results", function (assert) {
        searchVm.results([]);

        var result = searchVm.activeEmployeeSalaries();

        assert.strictEqual(result.length, 0);
    });
})();