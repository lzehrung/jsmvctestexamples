﻿/// <reference path="qunitTestDependencies.js"/>
/// <reference path="../../../JsTestExamples/Scripts/calculator/calculator.js"/>

// surrounding tests with this to hide calcVm from global scope
(function () {
    var calcVm;

    QUnit.module("QUnitCalculatorViewModel add",
    {
        beforeEach: function () {
            calcVm = new CalculatorViewModel();
        }
    });

    QUnit.test("two positive numbers", function (assert) {
        var result = calcVm.add(2, 2);

        assert.strictEqual(result, 4);
    });

    QUnit.test('two negative numbers', function (assert) {
        var result = calcVm.add(-2, -2);

        assert.strictEqual(result, -5, 'intentional failure!');
    });

    QUnit.test('one negative one positive', function (assert) {
        var result = calcVm.add(2, -2);

        assert.strictEqual(result, 0);
    });

    QUnit.test('adds to results list', function (assert) {
        calcVm.add(2, 3);

        var results = calcVm.results();
        assert.strictEqual(results.length, 1);
        assert.strictEqual(results[0], '2 + 3 = 5');
    });


    QUnit.module("QUnitCalculatorViewModel subtract",
    {
        beforeEach: function () {
            calcVm = new CalculatorViewModel();
        }
    });

    QUnit.test('two positive numbers', function (assert) {
        var result = calcVm.subtract(1, 2);

        assert.equal(result, -1);
    });

    QUnit.test('two negative numbers', function (assert) {
        var result = calcVm.subtract(-2, -2);

        assert.equal(result, 0);
    });

    QUnit.test('one negative one positive', function (assert) {
        var result = calcVm.subtract(2, -2);

        assert.equal(result, 4);
    });

    QUnit.test('adds to results list', function (assert) {
        calcVm.subtract(6, 3);

        var results = calcVm.results();
        assert.equal(results.length, 1);
        assert.equal(results[0], '6 - 3 = 3');
    });
})();