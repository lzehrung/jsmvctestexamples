﻿/// <reference path="qunitTestDependencies.js"/>
/// <reference path="../../../JsTestExamples/Scripts/employees/employeeSearch.js"/>
/// <reference path="../../../JsTestExamples/Scripts/employees/salarySummary.js"/>

// surrounding tests with this to hide calcVm from global scope
(function () {
    var reportVm;
    var employeeSearchVm;

    QUnit.module("QUnitSalarySummaryViewModel",
    {
        beforeEach: function () {
            employeeSearchVm = new EmployeeSearchViewModel();
            reportVm = new SalarySummaryViewModel(employeeSearchVm);
        },
        afterEach: function () {

        }
    });

    QUnit.test("salarySum is 0 when there are no active employees", function (assert) {
        Dexter.fake(employeeSearchVm, 'activeEmployeeSalaries', function() { return [] });

        var result = reportVm.salarySum();

        assert.strictEqual(result, 0);
    });

    QUnit.test("salarySum is 3.50 when there is a 2.0 and a 1.5 salary", function (assert) {
        Dexter.fake(employeeSearchVm, 'activeEmployeeSalaries', function () { return [2.0, 1.5] });

        var result = reportVm.salarySum();

        assert.strictEqual(result, 3.5);
    });

})();