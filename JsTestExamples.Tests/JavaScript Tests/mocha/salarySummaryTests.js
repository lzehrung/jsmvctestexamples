﻿/// <reference path="mochaTestDependencies.js"/>
/// <reference path="../../../JsTestExamples/Scripts/employees/employeeSearch.js"/>
/// <reference path="../../../JsTestExamples/Scripts/employees/salarySummary.js"/>

describe("MochaSalarySummaryViewModel", function () {

    var reportVm;
    var employeeSearchVm;

    beforeEach(function () {
        employeeSearchVm = new EmployeeSearchViewModel();
        reportVm = new SalarySummaryViewModel(employeeSearchVm);
    });
    
    describe("salarySum", function () {

        it("is 0 when there are no active employees", function () {
            Dexter.fake(employeeSearchVm, 'activeEmployeeSalaries', function () { return [] });

            var result = reportVm.salarySum();

            assert.strictEqual(result, 0);
        });

        it("is 3.50 when there is a 2.0 and a 1.5 salary", function() {
            Dexter.fake(employeeSearchVm, 'activeEmployeeSalaries', function () { return [2.0, 1.5] });

            var result = reportVm.salarySum();

            assert.strictEqual(result, 3.5);
        });

    });
});