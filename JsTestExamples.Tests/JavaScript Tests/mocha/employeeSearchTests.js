﻿/// <reference path="mochaTestDependencies.js"/>
/// <reference path="../../../JsTestExamples/Scripts/employees/employeeSearch.js"/>

describe("MochaEmployeeSearchViewModel", function () {

    var searchVm;

    beforeEach(function () {
        searchVm = new EmployeeSearchViewModel();
    });

    afterEach(function () {
        $.mockjax.clear();
    });

    describe("getSearchResults", function () {

        it("makes a request with the search query", function (done) {

            $.mockjax({
                url: "/Employee/Search",
                status: 200,
                responseText: [
                    { id: 0, name: "Jordan Walters" },
                    { id: 1, name: "David Richards" }
                ]
            });

            var ajaxPromise = searchVm.getSearchResults();

            ajaxPromise.then(function (data) {
                // success
                var results = searchVm.results();

                assert.strictEqual(results.length, 2);

                var firstResult = results[0];
                assert.strictEqual(firstResult.name, "Jordan Walters");
                done();
            }, function (jqXHR, textStatus, error) {
                // failure
                assert.isNotOk(jqXHR, "request failed unexpectedly");
                done();
            });
        });

        it("handles failed requests", function (done) {

            $.mockjax({
                url: "/Employee/Search",
                status: 500,
                responseText: "Ya done goofed!"
            });

            var ajaxPromise = searchVm.getSearchResults();

            ajaxPromise.then(function (data) {
                // success
                assert.isNotOk("Search succeeded, miraculously.");
                done();
            }, function (jqXHR, textStatus, error) {
                // failure
                assert.strictEqual(jqXHR.status, 500);

                assert.isTrue(searchVm.searchError());
                done();
            });
        });
    });

    describe("activeEmployeeSalaries", function () {

        it("returns empty array given no search results", function() {
            searchVm.results([]);

            var result = searchVm.activeEmployeeSalaries();

            assert.strictEqual(result.length, 0);
        });

        it("returns empty array given no search results", function () {
            searchVm.results([]);

            var result = searchVm.activeEmployeeSalaries();

            assert.strictEqual(result.length, 0);
        });

    });
});