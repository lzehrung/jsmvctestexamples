﻿/// <reference path="mochaTestDependencies.js"/>
/// <reference path="../../../JsTestExamples/Scripts/calculator/calculator.js"/>

describe('MochaCalculatorViewModel', function () {
    
    var calcVm;

    beforeEach(function () {
        calcVm = new CalculatorViewModel();
    });

    describe('add', function () {
        it('two positive numbers', function () {
            var result = calcVm.add(2, 2);

            assert.strictEqual(result, 4);
        });

        it('two negative numbers', function () {
            var result = calcVm.add(-2, -2);

            assert.strictEqual(result, -5, 'intentional failure!');
        });

        it('one negative one positive', function () {
            var result = calcVm.add(2, -2);

            assert.strictEqual(result, 0);
        });

        it('adds to results list', function () {
            calcVm.add(2, 3);

            var results = calcVm.results();
            assert.strictEqual(results.length, 1);
            assert.strictEqual(results[0], '2 + 3 = 5');
        });
    });

    describe("subtract", function () {
        it('two positive numbers', function () {
            var result = calcVm.subtract(1, 2);

            assert.strictEqual(result, -1);
        });

        it('two negative numbers', function () {
            var result = calcVm.subtract(-2, -2);

            assert.strictEqual(result, 0);
        });

        it('one negative one positive', function () {
            var result = calcVm.subtract(2, -2);

            assert.strictEqual(result, 4);
        });

        it('adds to results list', function () {
            calcVm.subtract(6, 3);

            var results = calcVm.results();
            assert.strictEqual(results.length, 1);
            assert.strictEqual(results[0], '6 - 3 = 3');
        });
    });
});