﻿/// <reference path="jasmineTestDependencies.js"/>
/// <reference path="../../../JsTestExamples/Scripts/employees/employeeSearch.js"/>
/// <reference path="../../../JsTestExamples/Scripts/employees/salarySummary.js"/>

describe("JasmineSalarySummaryViewModel", function () {

    var reportVm;
    var employeeSearchVm;

    beforeEach(function () {
        employeeSearchVm = new EmployeeSearchViewModel();
        reportVm = new SalarySummaryViewModel(employeeSearchVm);
    });

    describe("salarySum", function () {

        it("is 0 when there are no active employees", function () {
            spyOn(employeeSearchVm, 'activeEmployeeSalaries').and.returnValue([]);

            var result = reportVm.salarySum();

            expect(result).toBe(0);
        });

        it("is 3.50 when there is a 2.0 and a 1.5 salary", function () {
            spyOn(employeeSearchVm, 'activeEmployeeSalaries').and.returnValue([2.0, 1.5]);

            var result = reportVm.salarySum();

            expect(result).toBe(3.5);
        });

    });

    describe("isVisible (rate limited observable)", function () {
        it("has a subscriber that increments isVisibleChangeCount whenever isVisible changes", function (done) {
            
            setTimeout(function () {
                reportVm.isVisible(false);
            }, 1);

            setTimeout(function () {
                reportVm.isVisible(true);
            }, 70);

            setTimeout(function () {
                reportVm.isVisible(false);
            }, 140);

            setTimeout(function() {
                expect(reportVm.isVisibleChangeCount()).toBe(3);
                done();
            }, 500);
        }, 1000);
    });
});