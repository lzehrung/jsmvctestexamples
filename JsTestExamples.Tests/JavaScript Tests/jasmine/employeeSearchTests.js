﻿/// <reference path="jasmineTestDependencies.js"/>
/// <reference path="../../../JsTestExamples/Scripts/employees/employeeSearch.js"/>

describe("JasmineEmployeeSearchViewModel", function () {

    var searchVm;

    beforeEach(function () {
        jasmine.Ajax.install();

        searchVm = new EmployeeSearchViewModel();
    });

    afterEach(function () {
        jasmine.Ajax.uninstall();
    });

    describe("getSearchResults", function () {

        it("makes a request with the search query", function (done) {

            jasmine.Ajax.stubRequest('/Employee/Search').andReturn({
                status: 200,
                responseText: JSON.stringify([
                    { id: 0, name: "Jordan Walters" },
                    { id: 1, name: "David Richards" }
                ])
            });

            var ajaxPromise = searchVm.getSearchResults();

            ajaxPromise.then(function (data) {
                // success
                var results = searchVm.results();

                expect(results.length).toBe(2);

                var firstResult = results[0];
                expect(firstResult.name).toBe("Jordan Walters");
                done();
            }, function (jqXHR, textStatus, error) {
                // failure
                expect(jqXHR).toBeFalsy();
                done();
            });
        });

        it("handles failed requests", function (done) {

            jasmine.Ajax.stubRequest('/Employee/Search').andReturn({
                status: 500,
                responseText: "Ya done goofed!"
            });

            var ajaxPromise = searchVm.getSearchResults();

            ajaxPromise.then(function (data) {
                // success
                expect("Search succeeded, miraculously.").toBeFalsy();
                done();
            }, function (jqXHR, textStatus, error) {
                // failure
                expect(jqXHR.status).toBe(500);

                expect(searchVm.searchError()).toBeTruthy();
                done();
            });
        });
    });

    describe("activeEmployeeSalaries", function () {

        it("returns empty array given no search results", function() {
            searchVm.results([]);

            var result = searchVm.activeEmployeeSalaries();

            expect(result.length).toBe(0);
        });

        it("returns empty array given no search results", function () {
            searchVm.results([]);

            var result = searchVm.activeEmployeeSalaries();

            expect(result.length).toBe(0);
        });

    });
});