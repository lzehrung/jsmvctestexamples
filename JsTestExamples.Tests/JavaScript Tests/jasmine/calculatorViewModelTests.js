﻿/// <reference path="jasmineTestDependencies.js"/>
/// <reference path="../../../JsTestExamples/Scripts/calculator/calculator.js"/>

describe('JasmineCalculatorViewModel', function () {

    var calcVm;

    beforeEach(function () {
        calcVm = new CalculatorViewModel();
    });

    describe('add', function () {
        it('two positive numbers', function () {
            var result = calcVm.add(2, 2);

            expect(result).toBe(4);
        });

        it('two negative numbers', function () {
            var result = calcVm.add(-2, -2);

            expect(result).toBe(-5, 'intentional failure!');
        });

        it('one negative one positive', function () {
            var result = calcVm.add(2, -2);

            expect(result).toBe(0);
        });

        it('adds to results list', function () {
            calcVm.add(2, 3);

            var results = calcVm.results();
            expect(results.length).toBe(1);
            expect(results[0]).toBe('2 + 3 = 5');
        });
    });

    describe("subtract", function () {
        it('two positive numbers', function () {
            var result = calcVm.subtract(1, 2);

            expect(result).toBe(-1);
        });

        it('two negative numbers', function () {
            var result = calcVm.subtract(-2, -2);

            expect(result).toBe(0);
        });

        it('one negative one positive', function () {
            var result = calcVm.subtract(2, -2);

            expect(result).toBe(4);
        });

        it('adds to results list', function () {
            calcVm.subtract(6, 3);

            var results = calcVm.results();
            expect(results.length).toBe(1);
            expect(results[0]).toBe('6 - 3 = 3');
        });
    });
});