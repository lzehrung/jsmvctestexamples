﻿function SalarySummaryViewModel(employeeSearchVm) {
    var self = this;

    self.isVisible = ko.observable(true).extend({ rateLimit: 50 });
    self.isVisibleChangeCount = ko.observable(0);
    self.isVisible.subscribe(function(newValue) {
        self.isVisibleChangeCount(self.isVisibleChangeCount() + 1);
    });

    self.salarySum = ko.pureComputed(function () {
        var sum = 0;
        var salaries = employeeSearchVm.activeEmployeeSalaries();
        for (var i = 0; i < salaries.length; i++) {
            sum += salaries[i];
        }
        return sum;
    }, self);
}