﻿function EmployeeSearchViewModel() {
    var self = this;

    self.searchQuery = ko.observable();

    self.results = ko.observableArray();
    self.searchError = ko.observable(false);

    self.activeEmployeeSalaries = ko.pureComputed(function () {
        var activeEmployees = ko.utils.arrayFilter(self.results(), function (employee) {
            return employee.isActive();
        });
        var salaries = ko.utils.arrayMap(activeEmployees, function (employee) {
            return employee.salary();
        });
        return salaries;
    }, self);

    self.getSearchResults = function () {
        return $.ajax({
            url: '/Employee/Search',
            data: {
                query: self.searchQuery()
            }
        })
        .done(function (response) {
            self.results(response);
        })
        .fail(function (jqXHR, textStatus, error) {
            self.searchError(true);
        });
    }

    self.reset = function () {
        self.searchQuery('');
        self.results([]);
    }
}

function Employee(id, name, salary, active) {
    this.id = id;
    this.name = name;
    this.salary = salary;
    this.isActive = active;
}