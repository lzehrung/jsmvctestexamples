﻿function CalculatorViewModel() {
    var self = this;

    self.results = ko.observableArray();

    self.add = function (left, right) {
        var result = left + right;
        self.results.push(left + " + " + right + " = " + result);
        return result;
    }

    self.subtract = function (left, right) {
        var result = left - right;
        self.results.push(left + " - " + right + " = " + result);
        return result;
    }
}